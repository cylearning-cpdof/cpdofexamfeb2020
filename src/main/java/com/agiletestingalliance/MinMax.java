package com.agiletestingalliance;

public class MinMax {

    public int findMax(int first, int second) {
        if (second > first) {
            return second;
        } else {
            return first;
        }
    }

    public String bar(String string) {
        return string;
}

}
