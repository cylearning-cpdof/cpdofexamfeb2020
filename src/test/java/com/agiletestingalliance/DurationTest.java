package com.agiletestingalliance;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class DurationTest {

    private Duration duration;

    @Before
    public void setUp() throws Exception {
        duration = new Duration();
    }

    @Test
    public void dur() {
        final String durationDesc = duration.dur();
        assertTrue("Duration desc is wrong", durationDesc.contains("you can opt for either half days course or  full days programs"));
    }

    @Test
    public void calculateIntValue() {
        final int intValue = duration.calculateIntValue();
        assertEquals("Int Value is wrong", 5, intValue);
    }
}