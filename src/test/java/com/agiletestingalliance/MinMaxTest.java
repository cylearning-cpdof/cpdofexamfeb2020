package com.agiletestingalliance;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class MinMaxTest {

    private MinMax minMax;

    @Before
    public void setUp() throws Exception {
        minMax = new MinMax();
    }

    @Test
    public void findMaxFirstIsMax() {
        final int max = minMax.findMax(10, 1);
        assertEquals("Failed when first was max", 10, max);
    }

    @Test
    public void findMaxSecondIsMax() {
        final int max = minMax.findMax(1, 10);
        assertEquals("Failed when second was max", 10, max);
    }

    @Test
    public void bar() {
        final String givenString = "Some string";
        final String returnedString = minMax.bar(givenString);
        assertEquals("String returned not same as given", givenString, returnedString);
    }
}