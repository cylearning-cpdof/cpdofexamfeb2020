package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class TestClassTest {

    @Test
    public void gstr() {
        final String givenString = "Some String";
        final TestClass testClass = new TestClass(givenString);
        final String returnedString = testClass.gstr();
        assertEquals("Given string not same as returned string", givenString, returnedString);
    }
}