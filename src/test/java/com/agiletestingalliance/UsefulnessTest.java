package com.agiletestingalliance;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class UsefulnessTest {

    private Usefulness usefulness;

    @Before
    public void setUp() throws Exception {
        usefulness = new Usefulness();
    }

    @Test
    public void desc() {
        final String desc = usefulness.desc();
        assertTrue("Usefulness description is wrong", desc.contains("DevOps is about transformation, "));
    }

    @Test
    public void functionWF() {
        // Check that the method doesn't fail
        usefulness.functionWF();
        assertTrue(true);
    }
}