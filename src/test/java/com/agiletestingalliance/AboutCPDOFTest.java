package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class AboutCPDOFTest {

    @Test
    public void desc() {
        final String desc = new AboutCPDOF().desc();
        assertTrue("Description is wrong", desc.contains("CP-DOF is the only globally recognized certification program"));
    }
}